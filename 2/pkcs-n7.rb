#!/usr/bin/env ruby

class PKCS7
  private
  def self.pad_block(block, block_length)
    raise 'block length loo big!' unless block.length <= block_length
    diff = block_length - block.length
    padding = [diff].pack('C')*diff
    return block + padding
  end

  def self.unpad_block(block)
    padding = block[-1].unpack('C')[0]
    block[0, block.size - padding]
  end

  def self.get_data(input)
    if input.is_a?(File) then
      return input.read
    elsif input.is_a?(String) then
      return input
    else
      raise 'bad input type'
    end
  end

  public
  def self.pad_text(input, block_length)
    data = get_data(input).scan(/[\S\s]{#{block_length},#{block_length}}/)
    raise 'no data' if data.empty?
    if data[-1].size < block_length then
      data[-1] = pad_block(data[-1], block_length)
    else
      data += [pad_block('', block_length)]
    end
    return data.join
  end

  def self.unpad_text(input, block_length)
    data = get_data(input).scan(/[\S\s]{#{block_length},#{block_length}}/)
    if data[-1][-1].unpack('C') == block_length then # whole block
      data.delete_at(-1)
    else
      data[-1] = unpad_block(data[-1])
    end
    return data.join
  end
end

#p pad_text(ARGV[0], ARGV[1].to_i)

