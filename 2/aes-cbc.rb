#!/usr/bin/env ruby

require './pkcs-n7.rb'
require '../1/aes-ecb.rb'

$cipher_len = 128/8
class AES_CBC
  private
  def self.xor_strings(s0, s1)
    raise 'bad lengths: ' + s0.size.to_s + " vs " + s1.size.to_s unless s0.size == s1.size
    s0.unpack('C*').zip(s1.unpack('C*')).map{ |i0, i1| i0^i1 }.pack('C*')
  end

  public
  def self.decrypt_aes_cbc_128(string, key, iv = "\x00"*$cipher_len)
    raise 'bad iv' unless iv.size == $cipher_len
    raise 'bad key' unless key.size == $cipher_len
    cipher_blocks = string.scan(/[\S\s]{#{$cipher_len},#{$cipher_len}}/)
    prev_cipher = iv
    original = cipher_blocks.reduce('') do |orig, cipher|
      orig += xor_strings(AES_ECB.decrypt_aes_ecb_128(cipher, key, false), prev_cipher)
      prev_cipher = cipher
      orig
    end
    PKCS7.unpad_text(original, $cipher_len)
  end

  def self.encrypt_aes_cbc_128(string, key, iv = "\x00"*$cipher_len)
    raise 'bad iv' unless iv.size == $cipher_len
    raise 'bad key' unless key.size == $cipher_len
    cipher_blocks = PKCS7.pad_text(string, $cipher_len).scan(/[\S\s]{#{$cipher_len},#{$cipher_len}}/)
    prev_cipher = iv
    cipher_blocks.reduce('') do |cipher, orig|
      prev_cipher = AES_ECB.encrypt_aes_ecb_128(xor_strings(prev_cipher, orig), key, false)
      cipher += prev_cipher
    end
  end
end

#key='yellow submarine'
#iv="\x00"*$cipher_len
#print decrypt_aes_cbc_128(encrypt_aes_cbc_128(File.open(ARGV[0]).read, key, iv), key, iv)
