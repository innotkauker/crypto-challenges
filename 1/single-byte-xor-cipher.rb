#!/usr/bin/env ruby

def hex_to_intarray(s)
  s.scan(/../).map{ |s| s.hex }
end

def fixed_xor(s0, s1)
  raise 'two strings please' unless (s0.is_a?(String)) and (s1.is_a?(String))
  hex_to_intarray(s0).zip(hex_to_intarray(s1)).map{ |x,y| x^y }.map{ |s| s.to_s(16).rjust(2,'0') }.join
end

def decrypt(cipher, key)
  extended_key = key*cipher.size
  fixed_xor(cipher, extended_key.unpack('H*')[0])
end

def all_variants(cipher)
  result = []
  raise 'hex-encoded string please' unless cipher.is_a?(String)
  'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'.chars.each do |key|
    result << [decrypt(cipher, key)].pack('H*')
  end
  return result
end

def metric(text)
  text.chars.inject(0) do |result,c|
    if c[/[a-zA-Z ]/] == c
      result + 1
    else
      result
    end
  end
end

def best_variant(variants)
  variants.map!{ |x| { i: metric(x), text: x } }.sort!{ |x,y| y[:i] <=> x[:i] }[0]
end

print best_variant(all_variants(ARGV[0]))[:text]
