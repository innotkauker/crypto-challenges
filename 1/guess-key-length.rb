#!/usr/bin/env ruby

def xor(b0, b1)
  raise 'two ints please' unless (b0.is_a?(Integer)) and (b1.is_a?(Integer))
  b0^b1
end

def to_bitarray(str)
  str.chars.map{ |b| b.unpack('C')[0].to_s(2).rjust(8, '0') }.join.scan(/./)
end

def hamming_distance(s0, s1)
  raise 'two strings please' unless (s0.is_a?(String)) and (s1.is_a?(String))
  raise 'strings of equal length' unless s0.chars.length == s1.chars.length
  to_bitarray(s0).zip(to_bitarray(s1)).inject(0) do |result, bytes|
    if bytes[0] != bytes[1]
      result + 1
    else
      result
    end
  end
end

def get_bytes_from_file(bytes, file)
  enough_chars = bytes*2
  file.read(enough_chars).unpack('m*')[0].chars[0, bytes].join
end


def get_metric(keysize, filename, blocks_n)
  file = File.open(filename)
  #p file.read(keysize).unpack('m*')[0].chars.length
  #p file.read(keysize).unpack('m*')[0].chars.length
  blocks = []
  blocks_n.times { blocks << get_bytes_from_file(keysize, file) }
  blocks.permutation.inject(0) { |sum, perm| sum += hamming_distance(perm[0], perm[1]) }*1.0/blocks.length/keysize
end

p 2.upto(40).map { |i| [i, get_metric(i, ARGV[0], 4)] }.sort { |x, y| x[1] <=> y[1] }

