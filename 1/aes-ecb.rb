#!/usr/bin/env ruby

require 'openssl'

class AES_ECB
  def self.decrypt_aes_ecb_128(what, key, padding = true)
    decipher = OpenSSL::Cipher::new('AES-128-ECB')
    decipher.decrypt
    decipher.key = key
    decipher.padding = 0 unless padding
    decipher.update(what) + decipher.final
  end

  def self.encrypt_aes_ecb_128(what, key, padding = true)
    cipher = OpenSSL::Cipher::new('AES-128-ECB')
    cipher.encrypt
    cipher.key = key
    cipher.padding = 0 unless padding
    cipher.update(what) + cipher.final
  end
end

#puts decrypt_aes_ecb_128(ARGV[0], ARGV[1])

