#!/usr/bin/env ruby

def xor(b0, b1)
  raise 'two ints please' unless (b0.is_a?(Integer)) and (b1.is_a?(Integer))
  b0^b1
end

def encrypt_file(file, key)
  keystream = key.unpack('C*').cycle
  File.open(file).each_byte do |b|
    print xor(b, keystream.next).to_s(16).rjust(2, '0')
  end
end

encrypt_file(ARGV[0], ARGV[1])
